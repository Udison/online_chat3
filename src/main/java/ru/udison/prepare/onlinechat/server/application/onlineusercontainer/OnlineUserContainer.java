package ru.udison.prepare.onlinechat.server.application.onlineusercontainer;

import ru.udison.prepare.onlinechat.server.domain.User;

import java.util.List;

public interface OnlineUserContainer {

    void addToContainer(User user);
    List<User> getAllUsers();
    User getUserForNickName(String nickName) throws UserNotFoundException;
}
