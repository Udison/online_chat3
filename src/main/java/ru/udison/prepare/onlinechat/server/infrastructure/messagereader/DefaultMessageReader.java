/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.messagereader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Queue;
import ru.udison.prepare.onlinechat.server.application.logger.ExceptionLogger;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.infrastructure.converter.ConverterException;
import ru.udison.prepare.onlinechat.server.infrastructure.converter.MessageConverter;

/**
 *
 * @author Admin
 */
public class DefaultMessageReader implements Runnable {
    
    private Queue<Message> messagesToHandle;
    private BufferedReader in;
    private MessageConverter converter;

    public DefaultMessageReader(Queue<Message> messageToHandle, Socket socket) {
        this.messagesToHandle = messageToHandle;
        this.converter = new MessageConverter();
        try {
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            ExceptionLogger.writeLog(new Message(ex.getMessage()));
        }
    }

    @Override
    public void run() {
        while (true) {
            readMessage();
        }
    }

    private void readMessage() {
        try {
            messagesToHandle.add(new MessageConverter().convertStringToMessage(in.readLine()));
        } catch (IOException | ConverterException ex) {
            ExceptionLogger.writeLog(new Message(ex.getMessage()));
        }
    }
}
