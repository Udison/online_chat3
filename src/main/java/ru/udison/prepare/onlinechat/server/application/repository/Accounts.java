/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.application.repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;
import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.infrastructure.converter.UserConverter;

/**
 *
 * @author Admin
 */
public class Accounts {
    
    private File accountsFile;
    private static Accounts instance;
    private final ConcurrentHashMap<String, User> accounts;

    private Accounts() {
        accounts = new ConcurrentHashMap<>();
    }
    
    public static synchronized Accounts getInstance() {
        if (instance == null)
            return new Accounts();
        return instance;
    }

    public void uploadAcounts(String file) throws FileNotFoundException {
        accountsFile = new File(file);
        try (BufferedReader readerFromFile = new BufferedReader(new FileReader(accountsFile))) {
            String line;
            while ((line = readerFromFile.readLine()) != null) {
                User user = UserConverter.getInstace().convertStringToUser(line);
                accounts.putIfAbsent(user.getLogin(), user);
            }
        } catch (IOException ex) {
            //create exceptionhandler
        }
    }

    public ConcurrentHashMap<String, User> getAccounts() {
        return new ConcurrentHashMap<>(accounts);
    }

    public void addAccount(User user) {
        accounts.put(user.getLogin(), user);
        try (PrintWriter pw = new PrintWriter(accountsFile) ){
            pw.write(UserConverter.getInstace().convertUserToString(user));
        } catch (IOException ex) {
            // create ex handle
        }
    }

}
