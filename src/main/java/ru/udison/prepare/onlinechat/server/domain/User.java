package ru.udison.prepare.onlinechat.server.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

public class User {

    private String login;
    private String password;
    public String nickName;
    private LocalDate dateOfRegistration;
    private List<LocalDateTime>visitingHistory;

    public User(String login, String password, String nickName) {
        this.login = login;
        this.password = password;
        this.nickName = nickName;
        this.dateOfRegistration = LocalDate.now();
        this.visitingHistory = new ArrayList<LocalDateTime>();
    }

    public User() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public LocalDate getDateOfRegistration() {
        return dateOfRegistration;
    }

    public List<LocalDateTime> getVisitingHistory() {
        return visitingHistory;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.nickName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.nickName, other.nickName)) {
            return false;
        }
        return true;
    }
    
    
}
