package ru.udison.prepare.onlinechat.server.application.authentication;

import ru.udison.prepare.onlinechat.server.application.repository.Accounts;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.User;

public class AuthenticationImpl implements Authentication{

    @Override
    public String[] loginUser(Message message) {
        String[] values = message.getValue().split(" ");
        return values;
    }

    @Override
    public User defineUser(String login, String password) throws UserNotFoundException {
        User user = Accounts.getAccounts().get(login);
        if(password.equals(user.getPassword()))
            return user;
        throw new UserNotFoundException();
    }

    @Override
    public Message confirm(User user) {
        Message message = new Message("Welcom " + user.getNickName());
        return message;
    }

    @Override
    public Message refuse() {
        Message message = new Message("Login or password is wrong");
        return message;
    }
    
}
