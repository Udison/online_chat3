/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

/**
 *
 * @author Admin
 */
public class AuthenticationMessage extends Message {
    
    private final String ipSender;
    private final String login;
    private final String password;

    public AuthenticationMessage(String ipSender, String login, String password) {
        this.ipSender = ipSender;
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    
    
}
