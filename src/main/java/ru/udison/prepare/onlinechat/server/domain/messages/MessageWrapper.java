/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

/**
 *
 * @author Admin
 */
public class MessageWrapper {
    
    private final Actions action;
    private final String json;

    public MessageWrapper(Actions action, String json) {
        this.action = action;
        this.json = json;
    }
    
    public Actions getAction() {
        return action;
    }

    public String getJson() {
        return json;
    }
    
    
    
}
