/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.application.authentication;

import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.server.application.repository.Accounts;
import ru.udison.prepare.onlinechat.server.domain.messages.AuthenticationMessage;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.server.domain.messages.SystemMessage;
import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.AuthenticationConnectionsContainer;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.Connection;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.NonAuthenticationConnectionsContainer;
import ru.udison.prepare.onlinechat.server.infrastructure.converter.MessageConverter;

/**
 *
 * @author Admin
 */
public class AuthenticationHandler {
    
    private User user;
    private boolean userDefined = false;
    private boolean containerChanged = false;

    public AuthenticationHandler() {
    }
    
    public AuthenticationHandler defineUser(AuthenticationMessage message) {
        User u = Accounts.getInstance().getAccounts().get(message.getLogin());
        if (u == null) 
            return this;
        if(message.getPassword().equals(u.getPassword())){
            user = u;
            userDefined = true;
            return this;
        }
        return this;
    }
    
    public AuthenticationHandler changeConnectionContainer(Connection connection) {
        NonAuthenticationConnectionsContainer.getInstance().removeConnection(connection);
        connection.setUser(user);
        AuthenticationConnectionsContainer.getInstance().addConnection(connection);
        containerChanged = true;
        return this;
    }
    
    public SystemMessage notifyUser() {
        if (userDefined)
        return new SystemMessage(AuthenticationConnectionsContainer
                .getInstance()
                .getConnection(user)
                .getIp(), "SUCCESS");
        return new SystemMessage(AuthenticationConnectionsContainer
                .getInstance()
                .getConnection(user)
                .getIp(), "FAILED");
    }
}
