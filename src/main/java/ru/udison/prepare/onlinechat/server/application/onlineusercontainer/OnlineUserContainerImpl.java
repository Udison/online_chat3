package ru.udison.prepare.onlinechat.server.application.onlineusercontainer;

import ru.udison.prepare.onlinechat.server.domain.User;

import java.util.LinkedList;
import java.util.List;

public class OnlineUserContainerImpl implements OnlineUserContainer {

    private List<User>onlineUsers;

    public OnlineUserContainerImpl() {
        this.onlineUsers = new LinkedList<>();
    }

    @Override
    public void addToContainer(User user) {
        onlineUsers.add(user);

    }
    @Override
    public List<User> getAllUsers() {
        return onlineUsers;
    }
    @Override
    public User getUserForNickName(String nickName) throws UserNotFoundException {
            for (User user : onlineUsers) {
                if (user.getNickName().equals(nickName))
                    return user;
            }
            throw new UserNotFoundException();
    }
}
