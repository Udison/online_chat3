/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

/**
 *
 * @author Admin
 */
public enum Actions {
    
    AUTHENTICATION,
    REGISTRATION,
    TOUSER,
    TOALLUSERS,
    TOSYSTEM,
    TOSUPPORT,
    FROMSYSTEM,
    FROMUSER
}
