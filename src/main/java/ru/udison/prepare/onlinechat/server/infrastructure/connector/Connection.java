/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.connector;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Objects;
import java.util.Queue;
import ru.udison.prepare.onlinechat.server.application.logger.ExceptionLogger;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.domain.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.server.infrastructure.messagereader.DefaultMessageReader;
import ru.udison.prepare.onlinechat.server.infrastructure.messagemanager.MessageManager;

/**
 *
 * @author Admin
 */
public class Connection implements Runnable {

    
    private User user; 
    private final String ip;
    private final Socket socket;

    public Connection(Socket socket) {
        this.ip = socket.getInetAddress().getHostAddress();
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Queue<MessageWrapper> messagesToHandle = new ArrayDeque<>();
            new Thread(new DefaultMessageReader(messagesToHandle, socket)).start();
            new Thread(new MessageManager(messagesToHandle, this)).start();
        } catch (IOException ex) {
            ExceptionLogger.writeLog(new Message(ex.getMessage()));
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIp() {
        return ip;
    }

    public Socket getSocket() {
        return socket;
    }
    
    public void disonnect() {
        Thread.currentThread().interrupt();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.user);
        hash = 17 * hash + Objects.hashCode(this.ip);
        hash = 17 * hash + Objects.hashCode(this.socket);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Connection other = (Connection) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.ip, other.ip)) {
            return false;
        }
        if (!Objects.equals(this.socket, other.socket)) {
            return false;
        }
        return true;
    }
}
