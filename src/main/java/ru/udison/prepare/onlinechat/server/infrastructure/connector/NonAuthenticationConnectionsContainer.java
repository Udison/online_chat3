/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.connector;

import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Admin
 */
public class NonAuthenticationConnectionsContainer {
    private static volatile NonAuthenticationConnectionsContainer instance;
    private ConcurrentHashMap<String, Connection> connections;
    
    private NonAuthenticationConnectionsContainer() {
        connections = new ConcurrentHashMap<>();
    }
    
    public ConcurrentHashMap<String, Connection> getAllConnections() {
        if(connections != null) {
            ConcurrentHashMap<String, Connection> connectionsToGet = new ConcurrentHashMap<>(connections);
            return connectionsToGet;
        }
        else {
            connections = new ConcurrentHashMap<>();
            ConcurrentHashMap<String, Connection> connectionsToGet = new ConcurrentHashMap<>(connections);
            return connectionsToGet;            
        }
    }
    
    public void removeAllConnections(){
        if(connections != null) {
            connections.clear();
        }
        else {
            connections = new ConcurrentHashMap<>();
            connections.clear();
        }
    }

    public void addConnection(Connection connection){
        if(connections != null) {
            connections.put(connection.getIp(), connection);
        }
        else {
            connections = new ConcurrentHashMap<>();
            connections.put(connection.getIp(), connection);
        }
    }
    
    public void removeConnection(Connection connection){
            connections.remove(connection.getIp());
    }
    
    public Connection getConnection(String ip) {
        return connections.get(ip);
    }
    
    public static synchronized NonAuthenticationConnectionsContainer getInstance() {
        if(instance == null)
            return new NonAuthenticationConnectionsContainer();
        return instance;
    }
}
