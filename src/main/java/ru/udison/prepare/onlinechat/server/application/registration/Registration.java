package ru.udison.prepare.onlinechat.server.application.registration;


import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.User;

public interface Registration {

    String[] takeValues(Message m);

    boolean checkNickName(String nick);

    boolean checkLogin(String login);

    boolean checkPassword(String pasword);

    User finishRegistration(String login, String password, String nickName);

}
