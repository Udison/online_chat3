/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.application.exceptionmanager;

import java.time.LocalDateTime;
import jdk.nashorn.internal.objects.annotations.Getter;

/**
 *
 * @author Admin
 */
public class ExceptionWrapper {
    
    
    private final TypeException type;
    private final String value;
    private final LocalDateTime timeException;

    public ExceptionWrapper(TypeException type, String value, LocalDateTime timeException) {
        this.type = type;
        this.value = value;
        this.timeException = timeException;
    }
    
    
    
    
    
    private enum TypeException {
    
        USER_CONVERTER_EXCEPTION,
        MESSAGE_CONVERTER_EXCEPTION,
        FILE_NOT_FOUND_EXCEPTION,
        IOEXCEPTION,
        
    }
}



