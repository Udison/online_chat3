/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

import ru.udison.prepare.onlinechat.server.domain.User;

/**
 *
 * @author Admin
 */
public class MessageToAllUsers extends Message {
    
    private final User userSender;
    private final String text;

    public MessageToAllUsers(User userSender, String text) {
        this.userSender = userSender;
        this.text = text;
    }

    public User getUserSender() {
        return userSender;
    }

    public String getText() {
        return text;
    }
    
    
    
}
