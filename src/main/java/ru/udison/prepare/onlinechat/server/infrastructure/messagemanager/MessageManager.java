/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.messagemanager;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.Queue;
import ru.udison.prepare.onlinechat.server.application.authentication.*;
import ru.udison.prepare.onlinechat.server.application.logger.ExceptionLogger;
import ru.udison.prepare.onlinechat.server.application.registration.*;
import ru.udison.prepare.onlinechat.server.application.repository.Accounts;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.domain.messages.AuthenticationMessage;
import ru.udison.prepare.onlinechat.server.domain.messages.SystemMessage;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.*;
import ru.udison.prepare.onlinechat.server.infrastructure.converter.*;

/**
 *
 * @author Admin
 */
public class MessageManager implements Runnable {

    private final Queue<MessageWrapper> messagesToHandle;
    private final MessageConverter converter;
    private BufferedWriter writer;
    private final BufferedWriter response;
    private final Connection connection;

    public MessageManager(Queue<MessageWrapper> messagesToHandle, Connection connection) throws IOException {
        this.messagesToHandle = messagesToHandle;
        this.converter = new MessageConverter();
        this.response = new BufferedWriter(new OutputStreamWriter(connection.getSocket().getOutputStream()));
        this.connection = connection;
    }

    @Override
    public void run() {
        
        while (true) {
            handleMessage(messagesToHandle.poll());
        }
    }

    private void handleMessage(MessageWrapper message) {
        switch (message.getAction()) {
            case AUTHENTICATION:
                AuthenticationMessage m = converter.convertWrapperToMessage(message);
                AuthenticationHandler authentication = new AuthenticationHandler();
                SystemMessage notify = authentication
                        .defineUser(m)
                        .changeConnectionContainer(connection)
                        .notifyUser();
                break;
            case REGISTRATION:
                registration(message);
                break;
            case TOUSER:
                sendToUser(message);
                break;
            case TOALLUSERS:
                sendToAllUsers(message);
                break;
            case QUIT:
                quit();
                break;
            case TOSUPPORT:
                break;
        }

    }

    private void authentication(Message message) {
        Authentication authentication = new AuthenticationImpl();
        String[] authenticationValues = authentication.loginUser(message);
        try {
            User user = authentication.defineUser(authenticationValues[0], authenticationValues[1]);
            Message m = authentication.confirm(user);
            String jsonResponse = converter.convertMessageToString(m);
            response.write(jsonResponse);
            response.flush();
            connection.setUser(user);
            sendToAllUsers(new Message(connection.getUser().getNickName() + ": Joined to chat"));
            AuthenticationConnectionsContainer.addConnection(connection);
            NonAuthenticationConnectionsContainer.removeConnection(connection);

        } catch (UserNotFoundException | ConverterException | IOException ex) {
            ExceptionLogger.writeLog(new Message(ex.getMessage()));
            try {
                Message m = authentication.refuse();
                String jsonResponse = converter.convertMessageToString(m);
                response.write(jsonResponse);
                response.flush();
            } catch (ConverterException | IOException e) {
                ExceptionLogger.writeLog(new Message(e.getMessage()));
            } finally {
                try {
                    response.close();
                } catch (IOException ioe) {
                    ExceptionLogger.writeLog(new Message(ioe.getMessage()));
                }
            }
        } finally {
            try {
                response.close();
            } catch (IOException ex) {
                ExceptionLogger.writeLog(new Message(ex.getMessage()));
            }
        }
    }

    private void registration(Message message) {
        Registration registration = new RegistrationImpl();
        String[] registrationValues = registration.takeValues(message);
        String login = registrationValues[0];
        String password = registrationValues[1];
        String nick = registrationValues[2];
        try {
            if (!registration.checkLogin(login)) {
                response.write(converter.convertMessageToString(new Message("FAILED")));
                response.write(converter.convertMessageToString(new Message("Login is used")));
            }
            if (!registration.checkPassword(password)) {
                response.write(converter.convertMessageToString(new Message("FAILED")));
                response.write(converter.convertMessageToString(new Message("Password is wrong")));
            }
            if (!registration.checkNickName(nick)) {
                response.write(converter.convertMessageToString(new Message("FAILED")));
                response.write(converter.convertMessageToString(new Message("NickName is used")));
            }
            User newUser = registration.finishRegistration(login, password, nick);

            response.write(converter.convertMessageToString(new Message("SUCCESS")));
            response.write(converter.convertMessageToString(new Message("Welcome " + nick)));
            connection.setUser(newUser);
            Accounts.addAccount(newUser);
            AuthenticationConnectionsContainer.addConnection(connection);
            NonAuthenticationConnectionsContainer.removeConnection(connection);
        } catch (ConverterException | IOException ex) {
            ExceptionLogger.writeLog(new Message(ex.getMessage()));
        } finally {
            try {
                response.close();
            } catch (IOException ex) {
                ExceptionLogger.writeLog(new Message(ex.getMessage()));
            }
        }
    }

    private void sendToAllUsers(Message message) {
        try {
            String toSend = converter.convertMessageToString(message);
            for (Map.Entry<User, Connection> entry : AuthenticationConnectionsContainer.getAllConnections().entrySet()) {
                writer = new BufferedWriter(new OutputStreamWriter(entry
                        .getValue()
                        .getSocket()
                        .getOutputStream()));
                writer.write(toSend);
            }
        } catch (ConverterException | IOException e) {
            ExceptionLogger.writeLog(new Message(e.getMessage()));
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                ExceptionLogger.writeLog(new Message(ex.getMessage()));
            }
        }
    }

    private void sendToUser(Message message) {
        try {
            String toSend = converter.convertMessageToString(message);
            writer = new BufferedWriter(new OutputStreamWriter(AuthenticationConnectionsContainer
                    .getAllConnections()
                    .get(message.getTo())
                    .getSocket()
                    .getOutputStream()));
            writer.write(toSend);
        } catch (ConverterException | IOException e) {
            ExceptionLogger.writeLog(new Message(e.getMessage()));
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                ExceptionLogger.writeLog(new Message(ex.getMessage()));
            }
        }
    }

    private void quit() {
        connection.disonnect();
        sendToAllUsers(new Message(connection.getUser().getNickName() + ": Left chat"));
    }

    private void sendToSupport(Message message) {

    }
}
