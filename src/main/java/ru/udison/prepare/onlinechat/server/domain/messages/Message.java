package ru.udison.prepare.onlinechat.server.domain.messages;

import java.time.LocalDateTime;

public class Message {
    
    private final LocalDateTime sendTime;

    public Message() {
        this.sendTime = LocalDateTime.now();
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }
    
    

    
}

