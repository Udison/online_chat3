/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.application.Registration;

/**
 *
 * @author Admin
 */
public class RegistationException extends Exception {

    public RegistationException() {
    }

    public RegistationException(String message) {
        super(message);
    }

    public RegistationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistationException(Throwable cause) {
        super(cause);
    }

    public RegistationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
