/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.converter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.server.application.logger.ExceptionLogger;
import ru.udison.prepare.onlinechat.server.domain.messages.Actions;
import ru.udison.prepare.onlinechat.server.domain.messages.AuthenticationMessage;
import ru.udison.prepare.onlinechat.server.domain.messages.ClientActions;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.messages.MessageToAllUsers;
import ru.udison.prepare.onlinechat.server.domain.messages.MessageWrapper;
import ru.udison.prepare.onlinechat.server.domain.messages.RegistrationMessage;
import ru.udison.prepare.onlinechat.server.domain.messages.SystemMessage;
import ru.udison.prepare.onlinechat.server.domain.messages.UserMessage;

/**
 *
 * @author Admin
 */
public class MessageConverter {

    private final ObjectMapper mapper;

    public MessageConverter() {
        mapper = new ObjectMapper();
    }

    public <T> T convertWrapperToMessage(MessageWrapper wrapper) throws ConverterException {
        try {
            switch (wrapper.getAction()) {
                case AUTHENTICATION:
                    return (T) mapper.readValue(wrapper.getJson(), AuthenticationMessage.class);
                case REGISTRATION:
                    return (T) mapper.readValue(wrapper.getJson(), RegistrationMessage.class);
                case TOUSER:
                    return (T) mapper.readValue(wrapper.getJson(), UserMessage.class);
                case TOALLUSERS:
                    return (T) mapper.readValue(wrapper.getJson(), MessageToAllUsers.class);
                case TOSYSTEM:
                    return (T) mapper.readValue(wrapper.getJson(), SystemMessage.class);
                case TOSUPPORT:
                    return (T) mapper.readValue(wrapper.getJson(), SystemMessage.class);
                default :
                    return null;
            }
        } catch (IOException exception) {
            throw new ConverterException();
        }
    }

    public MessageWrapper convertMessageToWrapper(Message message, Actions action) throws ConverterException {
        try {
            return new MessageWrapper(action, mapper.writeValueAsString(message));
        } catch (IOException ioe) {
            throw new ConverterException();
        }
    }
}
