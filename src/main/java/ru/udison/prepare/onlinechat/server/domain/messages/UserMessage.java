/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;

/**
 *
 * @author Admin
 */
public class UserMessage extends Message {
    
    private final User userSender;
    private final User userRecipient;
    private final String text;

    public UserMessage(User userSender, User userRecipient, String text) {
        this.userSender = userSender;
        this.userRecipient = userRecipient;
        this.text = text;
    }

    public User getUserSender() {
        return userSender;
    }

    public User getUserRecipient() {
        return userRecipient;
    }

    public String getText() {
        return text;
    }
    
    
    
}
