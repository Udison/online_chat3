package ru.udison.prepare.onlinechat.server.infrastructure.dto;

import ru.udison.prepare.onlinechat.server.domain.User;

public class UserDTO {
    private String nickName;

    public UserDTO(User user) {
        this.nickName = user.getNickName();
    }

    public String getUser() {
        return nickName;
    }
}
