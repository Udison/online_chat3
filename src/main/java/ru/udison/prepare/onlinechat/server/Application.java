package ru.udison.prepare.onlinechat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import ru.udison.prepare.onlinechat.server.application.repository.Accounts;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.Connection;
import ru.udison.prepare.onlinechat.server.infrastructure.connector.NonAuthenticationConnectionsContainer;

public class Application {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Server is running");
        Accounts.getInstance().uploadAcounts("Accounts.txt");
        System.out.println("Loading accounts...");
        

        while (true) {
            System.out.println("Waiting connecton...");
            Socket clientSocket = serverSocket.accept();
            Connection newConnection = new Connection(clientSocket);
            NonAuthenticationConnectionsContainer.addConnection(newConnection);
            new Thread(newConnection).start();
            System.out.println(clientSocket.getInetAddress().getHostAddress() + " is connected...");
        }
    }
}
