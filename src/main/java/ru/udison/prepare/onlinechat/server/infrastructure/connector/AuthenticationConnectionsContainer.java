/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.connector;

import java.util.concurrent.ConcurrentHashMap;
import ru.udison.prepare.onlinechat.server.domain.User;

/**
 *
 * @author Admin
 */
public class AuthenticationConnectionsContainer {
    
    private static volatile AuthenticationConnectionsContainer instance;
    
    private ConcurrentHashMap<User, Connection> connections;
    
    private AuthenticationConnectionsContainer() {
        connections = new ConcurrentHashMap<>();
    }
    
    public ConcurrentHashMap<User, Connection> getAllConnections() {
        if(connections != null) {
            ConcurrentHashMap<User, Connection> connectionsToGet = new ConcurrentHashMap<>(connections);
            return connectionsToGet;
        }
        else {
            connections = new ConcurrentHashMap<>();
            ConcurrentHashMap<User, Connection> connectionsToGet = new ConcurrentHashMap<>(connections);
            return connectionsToGet;            
        }
    }
    
    public void removeAllConnections(){
        if(connections != null) {
            connections.clear();
        }
        else {
            connections = new ConcurrentHashMap<>();
            connections.clear();
        }
    }

    public void addConnection(Connection connection){
        if(connections != null) {
            connections.put(connection.getUser(), connection);
        }
        else {
            connections = new ConcurrentHashMap<>();
            connections.put(connection.getUser(), connection);
        }
    }
    
    public void removeConnection(Connection connection){
            connections.remove(connection.getUser());
    }
    
    public Connection getConnection(User user) {
        return connections.get(user);
    }
    
    public static synchronized AuthenticationConnectionsContainer getInstance() {
        if(instance == null)
            return new AuthenticationConnectionsContainer();
        return instance;
    }
}
