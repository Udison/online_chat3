/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.domain.messages;

import java.time.LocalDateTime;


/**
 *
 * @author Admin
 */
public class SystemMessage extends Message{
    
    private final String ipRecipient;
    private final String response;

    public SystemMessage(String ipRecipient, String response) {
        this.ipRecipient = ipRecipient;
        this.response = response;
    }
    
}
