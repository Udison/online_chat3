/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.infrastructure.converter;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import ru.udison.prepare.onlinechat.server.domain.User;

/**
 *
 * @author Admin
 */
public class UserConverter {

    private static UserConverter converter;
    private final ObjectMapper mapper;

    private UserConverter() {
        mapper = new ObjectMapper();
    }

    public static synchronized UserConverter getInstace() {
        if (converter == null) {
            return new UserConverter();
        }
        return converter;
    }

    public synchronized User convertStringToUser(String json) throws IOException {
        return mapper.readValue(json, User.class);
    }
    
    public synchronized String convertUserToString(User user) throws IOException{
        return mapper.writeValueAsString(user);
    }
}
