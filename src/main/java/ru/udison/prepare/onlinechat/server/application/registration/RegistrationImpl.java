package ru.udison.prepare.onlinechat.server.application.registration;

import java.util.Map;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.User;
import ru.udison.prepare.onlinechat.server.application.repository.Accounts;

public class RegistrationImpl implements Registration {

    @Override
    public String[] takeValues(Message m) {
        String[] values = m.getValue().split(" ");
        return values;
    }

    @Override
    public boolean checkNickName(String nick) {
        
        if (nick.contains("\\W") && nick.length() > 16) {
            return false;
        }
        for (Map.Entry<String, User> entry : Accounts.getAccounts().entrySet()) {
            if (nick.equals(entry.getValue().getNickName())) {
                return false;
            }
        }
        return true;
    }
    

    @Override
    public boolean checkLogin(String login) {
        if (login.contains("\\W") && login.length() > 16) {
            return false;
        }
        for (Map.Entry<String, User> entry : Accounts.getAccounts().entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean checkPassword(String pasword) {
        if (pasword.contains("\\W") && pasword.length() > 25) {
            return false;
        }
        return true;
    }

    @Override
    public User finishRegistration(String login, String password, String nickName) {
        User user = new User(login, password, nickName);
        return user;
    }

}
