package ru.udison.prepare.onlinechat.server.application.authentication;

import ru.udison.prepare.onlinechat.server.domain.messages.Message;
import ru.udison.prepare.onlinechat.server.domain.User;

public interface Authentication {
    
    User defineUser(String login, String password) throws UserNotFoundException ;
    String[] loginUser(Message message);
    Message confirm(User user);
    Message refuse();
}
