/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.udison.prepare.onlinechat.server.application.logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import ru.udison.prepare.onlinechat.server.domain.messages.Message;

/**
 *
 * @author Admin
 */
public class ExceptionLogger {

    private static PrintWriter pw;
    private static File logFile;

    static {
        logFile = new File("Logs.txt");
        try {
            pw = new PrintWriter(logFile);
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        }
    }

    public static void writeLog(Message log) { //synchronized
        String timeLog = String.valueOf(log.getSendTime());
        String logToWrite = timeLog + " / " + log.getFrom() + " / " + log.getValue() + ".";
        pw.write(logToWrite);
        pw.close();
    }

}
